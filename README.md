# st2023-api-automation

## API automation
Backend = simple TODO app (see other repo)

Automation can be done with any technology stack, sample code is added to this repo for:
- Python with requests and pytest
- javaScript with Chai

Please feel free to add yours with a pull request :-)

## Run pytest
`pytest --html=report.html` will give you an example output like this:

```
 pytest --html=report.html
======================================================== test session starts ========================================================
platform win32 -- Python 3.10.8, pytest-7.4.1, pluggy-1.3.0
rootdir: C:\Users\johan.gabriels\workspace\st2023-api-automation\python
plugins: html-4.0.0, metadata-3.0.0
collected 4 items

test_add_task.py .                                                                                                             [ 25%]
test_status.py .                                                                                                               [ 50%]
test_user_creation.py ..                                                                                                       [100%]

------------- Generated html report: file://C:/Users/johan.gabriels/workspace/st2023-api-automation/python/report.html --------------
========================================================= 4 passed in 0.62s =========================================================
```


