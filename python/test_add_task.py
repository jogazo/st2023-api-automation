import json

import pytest
import requests
from requests.auth import HTTPBasicAuth

from settings import BASE_URL


def _assert_task_creation_data(task_data, expected_keys={'id', 'title', 'done'}):
    observed_keys = set(task_data.keys())
    # Using the difference operator `-` on sets
    assert not (expected_keys - observed_keys), \
        f'Unexpected data response when adding new task: {task_data}'


@pytest.mark.parametrize(
    'user, password, task_description',
    [
        ('testuser20230904232551', 'testuser20230904232551@resillion.com', 'Buy shampoo'),
    ]
)
def test_add_task_happy_flow(user, password, task_description, url_suffix='/api/v1/tasks'):
    data = {
        'title': task_description
    }

    test_url = f'{BASE_URL}{url_suffix}'
    print('about to add a task:', test_url)

    response = requests.post(test_url, data=json.dumps(data), auth=HTTPBasicAuth(user, password))
    print(response.json())
    assert response.status_code == 201, f'Unexpected status! {test_url} Expected 201, got: {response.status_code}'

    assert response.json()['success'], \
        f'Unexpected success state! {test_url} Expected True, got {response.json()["success"]}'
    assert 'The task was created successfully' == response.json()['message'], \
        f'Unexpected message! {test_url} Got: {response.json()["message"]}'
    _assert_task_creation_data(response.json()['data'])
