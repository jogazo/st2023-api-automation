from datetime import datetime
import json

import pytest
import requests

from settings import BASE_URL


@pytest.mark.parametrize(
    'first_name, last_name',
    [
        ('Test', f'User{datetime.now().strftime("%Y%m%d%H%M%S")}'),
    ]
)
def test_create_user_happy_flow(first_name, last_name, url_suffix='/api/v1/user'):
    user_name = f'{first_name.lower()}{last_name.lower()}'
    data = {
      'username': user_name,
      'email': f'{user_name}@resillion.com',
      'full_name': f'{first_name} {last_name}',
      'password': f'{user_name}@resillion.com'
    }

    test_url = f'{BASE_URL}{url_suffix}'
    print('about to create user:', test_url)
    print(json.dumps(data))
    response = requests.post(test_url, data=json.dumps(data))
    print(response.json())
    assert response.status_code == 200, f'Unexpected status! {test_url} Expected 200, got: {response.status_code}'

    assert response.json()['success'], \
        f'Unexpected success state! {test_url} Expected True, got {response.json()["success"]}'
    assert 'The user was register successfully' == response.json()['message'], \
        f'Unexpected status message! {test_url} Got: {response.json()["message"]}'


@pytest.mark.parametrize(
    'first_name, last_name',
    [
        ('Test', f'User{datetime.now().strftime("%Y%m%d%H%M%S")}'),
    ]
)
def test_create_user_invalid_email(first_name, last_name, url_suffix='/api/v1/user'):
    user_name = f'{first_name.lower()}{last_name.lower()}'
    data = {
      'username': user_name,
      'email': f'{user_name}@resillion',
      'full_name': f'{first_name} {last_name}',
      'password': f'{user_name}@resillion'
    }

    test_url = f'{BASE_URL}{url_suffix}'
    response = requests.post(test_url, data=json.dumps(data))
    print(response.json())
    assert response.status_code == 422, f'Unexpected status! {test_url} Expected 422, got: {response.status_code}'

    assert not response.json()['success'], \
        f'Unexpected success state! {test_url} Expected False, got {response.json()["success"]}'
    assert 'Schema validation error' == response.json()['message'], \
        f'Unexpected status message! {test_url} Got: {response.json()["message"]}'

    error_messages = response.json()['errors']
    assert error_messages[0] == '`email` value is not a valid email address', \
        f'Error message at index 0 is unexpected, got: {error_messages[0]}'
