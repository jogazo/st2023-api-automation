import requests

from settings import BASE_URL, EXPECTED_VERSION


def test_status(url_suffix='/api/v1/status'):
    test_url = f'{BASE_URL}{url_suffix}'
    print('about to get status:', test_url)
    response = requests.get(test_url)
    assert response.status_code == 200, f'Unexpected status! {test_url} Expected 200, got: {response.status_code}'

    assert response.json()['success'], \
        f'Unexpected success state! {test_url} Expected True, got {response.json()["success"]}'
    assert EXPECTED_VERSION == response.json()['version'], \
        f'Unexpected version! {test_url} Expected {EXPECTED_VERSION}, got {response.json()["version"]}'
    assert 'FASTAPI TODO APPLICATION' in response.json()['message'].upper(), \
        f'Unexpected status message! {test_url} Got: {response.json()["message"]}'
